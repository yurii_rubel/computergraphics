import { Component, OnInit } from '@angular/core';
import { IFS, LSystem } from 'fractals';
import type { TBounds } from 'fractals';

@Component({
  selector: 'app-fractals',
  templateUrl: './fractals.component.html',
  styleUrls: ['./fractals.component.css']
})

export class FractalsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

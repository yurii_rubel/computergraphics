import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutUsComponent } from './about-us/about-us.component';
import { AffineTransformationsComponent } from './affine-transformations/affine-transformations.component';
import { ColorModelsComponent } from './color-models/color-models.component';
import { FractalsComponent } from './fractals/fractals.component';
import { MainPageComponent } from './main-page/main-page.component';

const routes: Routes = [
  { path: 'about-us', component: AboutUsComponent },
  { path: 'affine-transformations', component: AffineTransformationsComponent },
  { path: 'color-models', component: ColorModelsComponent },
  { path: 'fractals', component: FractalsComponent },
  { path: '', component: MainPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

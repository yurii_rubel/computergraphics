import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AffineTransformationsComponent } from './affine-transformations.component';

describe('AffineTransformationsComponent', () => {
  let component: AffineTransformationsComponent;
  let fixture: ComponentFixture<AffineTransformationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AffineTransformationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AffineTransformationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

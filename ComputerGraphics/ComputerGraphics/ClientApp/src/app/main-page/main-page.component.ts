import { Component, OnInit } from '@angular/core';
import { FractalsContentComponent } from '../content/fractals-content/fractals-content.component';
import { ColorModelsContentComponent } from '../content/color-models-content/color-models-content.component';
import { AffineTransformationsContentComponent } from '../content/affine-transformations-content/affine-transformations-content.component'
import { CgComponent } from '../content/cg/cg.component'
import { faAngleDoubleDown } from '@fortawesome/free-solid-svg-icons';

declare var $: any;

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  ngOnInit() {
  $(document).ready(function() {
    $('.slider').slick({
      arrows: true,
      dots: true,
      slidesToShow: 1,
      autoplay: true,
      speed: 1500,
      autoplayspeed: 3500
    })
  })

    const fa = document.querySelector('.filter__arrow');
    if (fa) {
      const filterMenu = document.querySelector('.filter__menu');
      fa.addEventListener("click", function (e) {
        filterMenu.classList.toggle('active');
      });
    }
  };

  contentComponent = CgComponent;

  constructor() { }

  main_assignComponent(component) {
    if (component === "cg") this.contentComponent = CgComponent;
  }

  assignComponent(component) {
    if (component === "color-models") this.contentComponent = ColorModelsContentComponent;
    else if (component === "fractals") this.contentComponent = FractalsContentComponent;
    else this.contentComponent = AffineTransformationsContentComponent;
  }

  faAngleDoubleDown = faAngleDoubleDown;

}

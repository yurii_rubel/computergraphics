import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AffineTransformationsContentComponent } from './affine-transformations-content.component';

describe('AffineTransformationsContentComponent', () => {
  let component: AffineTransformationsContentComponent;
  let fixture: ComponentFixture<AffineTransformationsContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AffineTransformationsContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AffineTransformationsContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

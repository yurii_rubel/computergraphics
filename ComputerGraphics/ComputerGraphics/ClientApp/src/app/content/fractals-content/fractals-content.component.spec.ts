import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FractalsContentComponent } from './fractals-content.component';

describe('FractalsContentComponent', () => {
  let component: FractalsContentComponent;
  let fixture: ComponentFixture<FractalsContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FractalsContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FractalsContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColorModelsContentComponent } from './color-models-content.component';

describe('ColorModelsContentComponent', () => {
  let component: ColorModelsContentComponent;
  let fixture: ComponentFixture<ColorModelsContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ColorModelsContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ColorModelsContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

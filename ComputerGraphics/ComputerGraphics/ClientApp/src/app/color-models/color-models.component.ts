import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-color-models',
  templateUrl: './color-models.component.html',
  styleUrls: ['./color-models.component.css']
})

export class ColorModelsComponent implements OnInit {

  constructor() { }

  ngOnInit()
  {
    document.getElementById("getval").addEventListener("change", readURL, true);

    function readURL() {
      var file = (<HTMLInputElement>document.getElementById("getval")).files[0];
      var reader = new FileReader();
      reader.onloadend = function () {
        document.getElementById("clock").style.backgroundImage = "url(" + reader.result + ")";
      }
      if (file) {
        reader.readAsDataURL(file);
      } else { }
    }

    var canvas = (<HTMLCanvasElement>document.querySelector("clock"));

    var link = document.createElement('a');
    link.innerHTML = 'download image';
    link.addEventListener('click', function (ev) {
      link.href = canvas.toDataURL();
      link.download = "mypainting.png";
    }, false);
    document.body.appendChild(link);
  }
}

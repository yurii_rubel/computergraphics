import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColorModelsComponent } from './color-models.component';

describe('ColorModelsComponent', () => {
  let component: ColorModelsComponent;
  let fixture: ComponentFixture<ColorModelsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ColorModelsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ColorModelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

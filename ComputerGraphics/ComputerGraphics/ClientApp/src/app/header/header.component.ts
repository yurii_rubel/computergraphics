import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    const menuIcon = document.querySelector('.menu__icon');
    const menuLink_one = document.querySelector('.one');
    const menuLink_two = document.querySelector('.two');
    const menuLink_three = document.querySelector('.three');
    const menuLink_four = document.querySelector('.four');
    const menuLink_five = document.querySelector('.five');
    if (menuIcon) {
      const menuBody = document.querySelector('.menu__body');
      menuIcon.addEventListener("click", function (e) {
        document.body.classList.toggle('no-scroll');
        menuIcon.classList.toggle('active');
        menuBody.classList.toggle('active');
      }
      );
    }
    if (menuLink_one) {
      const menuBody = document.querySelector('.menu__body');
      menuLink_one.addEventListener("click", function (e) {
        menuIcon.classList.remove('active');
        menuBody.classList.remove('active');
        document.body.classList.remove('no-scroll');
      }
      );
    }
    if (menuLink_two) {
      const menuBody = document.querySelector('.menu__body');
      menuLink_two.addEventListener("click", function (e) {
        menuIcon.classList.remove('active');
        menuBody.classList.remove('active');
        document.body.classList.remove('no-scroll');
      }
      );
    }
    if (menuLink_three) {
      const menuBody = document.querySelector('.menu__body');
      menuLink_three.addEventListener("click", function (e) {
        menuIcon.classList.remove('active');
        menuBody.classList.remove('active');
        document.body.classList.remove('no-scroll');
      }
      );
    }
    if (menuLink_four) {
      const menuBody = document.querySelector('.menu__body');
      menuLink_four.addEventListener("click", function (e) {
        menuIcon.classList.remove('active');
        menuBody.classList.remove('active');
        document.body.classList.remove('no-scroll');
      }
      );
    }
    if (menuLink_five) {
      const menuBody = document.querySelector('.menu__body');
      menuLink_five.addEventListener("click", function (e) {
        menuIcon.classList.remove('active');
        menuBody.classList.remove('active');
        document.body.classList.remove('no-scroll');
      }
      );
    }
  }
}

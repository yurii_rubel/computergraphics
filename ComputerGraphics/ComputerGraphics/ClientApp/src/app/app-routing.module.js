"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.appRoutingModule = void 0;
var router_1 = require("@angular/router");
var about_us_1 = require("./about-us");
var affine_transformations_1 = require("./affine-transformations");
var color_models_1 = require("./color-models");
var fractals_1 = require("./fractals");
var main_page_1 = require("./main-page");
var routes = [
    { path: 'about-us', component: about_us_1.AboutUsComponent },
    { path: 'affine-transformations', component: affine_transformations_1.AffineTransformationsComponent },
    { path: 'color-models', component: color_models_1.ColorModelsComponent },
    { path: 'fractals', component: fractals_1.FractalsComponent },
    { path: '', component: main_page_1.MainPageComponent },
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];
exports.appRoutingModule = router_1.RouterModule.forRoot(routes);
//# sourceMappingURL=app-routing.module.js.map
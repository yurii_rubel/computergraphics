import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from "@angular/common/http";
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FractalsComponent } from './fractals/fractals.component';
import { ColorModelsComponent } from './color-models/color-models.component';
import { AffineTransformationsComponent } from './affine-transformations/affine-transformations.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MainPageComponent } from './main-page/main-page.component';
import { FractalsContentComponent } from './content/fractals-content/fractals-content.component';
import { ColorModelsContentComponent } from './content/color-models-content/color-models-content.component';
import { AffineTransformationsContentComponent } from './content/affine-transformations-content/affine-transformations-content.component';
import { CgComponent } from './content/cg/cg.component'

@NgModule({
  declarations: [
    AppComponent,
    FractalsComponent,
    ColorModelsComponent,
    AffineTransformationsComponent,
    AboutUsComponent,
    HeaderComponent,
    FooterComponent,
    MainPageComponent,
    FractalsContentComponent,
    ColorModelsContentComponent,
    AffineTransformationsContentComponent,
    CgComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    SlickCarouselModule,
    ReactiveFormsModule,
    HttpClientModule,
    NoopAnimationsModule,
    BrowserAnimationsModule,
    FormsModule,
    MatInputModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

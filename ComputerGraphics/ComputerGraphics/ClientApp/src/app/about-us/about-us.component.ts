import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

declare var $: any;
declare var swal: any;

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css'],
  styles: [`input.ng-touched.ng-invalid {border:solid red 2px;}
            input.ng-touched.ng-valid {border:solid green 2px;}`],
})

export class AboutUsComponent implements OnInit {

  pic1:string = "assets/img/Рисунок1.jpg";
  pic2: string = "assets/img/Рисунок2.jpg";

  myplaceHolder: string = "Введіть ім'я"
  myplaceHolder_one: string = "Введіть електрону пошту"
  myplaceHolder_two: string = "Введіть повідомлення"
  boolData: boolean = false;

  checkPlaceHolder() {
    if (this.myplaceHolder) {
      this.myplaceHolder = null
      return;
    } else {
      this.myplaceHolder = "Введіть ім'я"
      return
    }
  }

  checkPlaceHolder1() {
    if (this.myplaceHolder_one) {
      this.myplaceHolder_one = null
      return;
    } else {
      this.myplaceHolder_one = "Введіть електрону пошту"
      return
    }
  }

  checkPlaceHolder2() {
    if (this.myplaceHolder_two) {
      this.myplaceHolder_two = null
      return;
    } else {
      this.myplaceHolder_two = "Введіть повідомлення"
      return
    }
  }

  form: FormGroup;
  name: FormControl = new FormControl("", [Validators.required]);
  email: FormControl = new FormControl("", [Validators.required, Validators.email]);
  message: FormControl = new FormControl("", [Validators.required]);
  honeypot: FormControl = new FormControl(""); // we will use this to prevent spam
  submitted: boolean = false; // show and hide the success message
  isLoading: boolean = false; // disable the submit button if we're loading
  responseMessage: string; // the response message to show to the user

  constructor(private formBuilder: FormBuilder, private http: HttpClient) {
    this.form = this.formBuilder.group({
      name: this.name,
      email: this.email,
      message: this.message,
      honeypot: this.honeypot
    });
  }

  ngOnInit(): void {
  }

  onSubmit() {
    if (this.form.status == "VALID" && this.honeypot.value == "") {
      this.form.disable(); // disable the form if it's valid to disable multiple submissions
      var formData: any = new FormData();
      formData.append("name", this.form.get("name").value);
      formData.append("email", this.form.get("email").value);
      formData.append("message", this.form.get("message").value);
      this.isLoading = true; // sending the post request async so it's in progress
      this.submitted = false; // hide the response message on multiple submits
      this.http.post("https://script.google.com/macros/s/AKfycby9Ti5d__4ix4qVbDDjqOFrnGvh-ZmSYGhkiDy0Mw/exec", formData).subscribe(
        (response) => {
          // choose the response message
          if (response["result"] == "success") {
            swal({
              title: 'Успіх!',
              text: 'Повідомлення успішно надіслано!',
              type: 'success',
              confirmButtonText: 'Ок'
            })
            this.form.reset();
          } else {
            swal({
              title: 'Помилка!',
              text: 'Повідомлення не надіслано!',
              type: 'error',
              confirmButtonText: 'Ок'
            })
          }
          this.form.enable(); // re enable the form after a success
          this.submitted = true; // show the response message
          this.isLoading = false; // re enable the submit button
          console.log(response);
        },
        (error) => {
          this.responseMessage = "Oops! An error occurred... Reload the page and try again.";
          this.form.enable(); // re enable the form after a success
          this.submitted = true; // show the response message
          this.isLoading = false; // re enable the submit button
          console.log(error);
        }
      );
    }
  }
}
